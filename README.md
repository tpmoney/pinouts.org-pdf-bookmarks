# Pinouts.org PDF Bookmarks

This repo contains a bookmarks file to apply to the PDFs available at
pinouts.org[1]. The file is designed to be applied with JPdfBookmarks[2]
and can be applied as follows:

1. Install JPdfBookmarks[2] as instructed in the repo
2. Clone this repostory
3. Checkout the tag from this repository that corresponds to the version
   of the PDF you have. For example if you have V0.3 of the Pinouts PDF
   then you want to checkout tag `v0.3`
4. Run the following command, making substitutions as appropriate:

    ```shell
    jdfpbookmarks \
      --page-sep=';' \
      --out ${output_pdf_name}.pdf \
      --apply ${path_to_this_repo}/pinouts.org-bookmarks.txt \
      ${path_to_Pinouts_pdf}
    ```

This bookmarks file uses `;` as the page separator rather than the default
`/` character because a number of the items use `/` in their names.

[1]: https://pinouts.org
[2]: https://github.com/mavaddat/jpdfbookmarks
